const path = require('path');
const fs = require('fs');
const nunjucks = require('nunjucks');
const _ = require('lodash');
const SRC_DATA = path.join(__dirname, 'data', 'all-shows.json');
const sitePath = path.join(__dirname, 'site');
if (!fs.existsSync(sitePath)) {
    fs.mkdirSync(sitePath);
}
const OUTPUT_DEST = path.join(sitePath, 'index.html');

run();

function getDiffs() {
    const diffFiles = fs.readdirSync(path.join(__dirname, 'data')).filter(fname => {
        return fname.startsWith('diff-20') && fname.endsWith('.json');
    });
    diffFiles.sort().reverse();
    // keep only the latest 5 diffs
    const toRemove = diffFiles.slice(5, diffFiles.length);
    toRemove.forEach(fname => {
        let rpath = path.join(__dirname, 'data', fname);
        fs.unlinkSync(rpath);
    });

    const allDiffs = diffFiles.slice(0, 2).map(fname => {
        let dpath = path.join(__dirname, 'data', fname);
        const data = require(dpath);
        return data.filter(item => item.next).map(diff => {
            diff.next.diffDate = fname.split('diff-')[1].split('.json')[0];
            diff.next.diffType = diff.diffType;
            return diff.next;
        });
    });

    return _.chain(allDiffs).flatten().orderBy('diffDate').reverse().value();
}

function run() {
    const jsDate = new Date();
    const updated = `${jsDate.getUTCFullYear()}-${String(jsDate.getUTCMonth() + 1).padStart(2, '0')}-${String(jsDate.getUTCDate()).padStart(2, '0')}`;

    const diffs = getDiffs();
    // add some search terms
    let shows = JSON.parse(fs.readFileSync(SRC_DATA, 'utf-8')).map(show => {
        show.indexText = `${show.date};${show.headliner};${show.venue};`;
        if (show.support) {
            show.indexText += show.support;
        }
        const diff = diffs.find(diff => {
            return diff.headliner === show.headliner;
        });
        if (diff) {
            show.bgColor = "bg-gradient ";
            show.bgColor += diff.diffType === "new" ? "bg-success text-light" : "bg-info";
        }
        return show;
    });
    const rendered = nunjucks.render('template.html', {
        updated,
        shows,
        diffs
    });
    if (fs.existsSync(OUTPUT_DEST)) {
        fs.unlinkSync(OUTPUT_DEST);
    }
    fs.writeFileSync(OUTPUT_DEST, rendered);
}