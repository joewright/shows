#!/bin/bash
dt=$(date)
echo "$dt Gathering shows and creating web page"
node get-shows.js
node create-page.js
node create-calendar.js
dt=$(date)
echo "$dt Syncing with neocities"
neocities push site/
dt=$(date)
echo "$dt Done"