const fs = require('fs');
const path = require('path');
const SRC_DATA = path.join(__dirname, 'data', 'all-shows.json');
const urlMap = {
    "40 Watt Club": "https://www.40watt.com/",
    "529": "https://529atlanta.com/",
    "Aisle 5": "https://aisle5atl.com/",
    "Boggs SS": "https://www.boggssocial.com/   ",
    "Center Stage": "https://www.centerstage-atlanta.com/",
    "Cobb Energy Performing Arts Centre": "https://www.cobbenergycentre.com/",
    "District": "https://districtatlanta.com/",
    "Georgia Theatre": "https://www.georgiatheatre.com/",
    "Heaven + Purgatory  - Masquerade": "http://www.masqueradeatlanta.com/",
    "Heaven - Masquerade": "http://www.masqueradeatlanta.com/",
    "Hell - Masquerade": "http://www.masqueradeatlanta.com/",
    "Purgatory  - Masquerade": "http://www.masqueradeatlanta.com/",
    "Purgatory - Masquerade": "http://www.masqueradeatlanta.com/",
    "Star Bar": "https://www.starbaratl.bar/",
    "Terminal West": "https://terminalwestatl.com/",
    "The EARL": "https://badearl.com/",
    "The Eastern": "https://easternatl.com/",
    "The Loft": "https://www.centerstage-atlanta.com/",
    "The Tabernacle": "https://www.livenation.com/venue/KovZpaFEZe/tabernacle-events",
    "Variety Playhouse": "https://variety-playhouse.com/",
    "Vinyl": "https://www.centerstage-atlanta.com/",
}

function getVenueUrl(venue) {
    if (venue.includes(') ')) {
        venue = venue.split(') ').pop()
    }
    if (urlMap[venue]) {
        return urlMap[venue];
    } else if (venue.includes('Masquerade')) {
        return 'http://www.masqueradeatlanta.com/';
    }
    return ''
}

function calendarFromShows(shows) {
    // .ics template text
    let calendarText = `BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//ATL Shows//NONSGML Event Calendar//EN
URL:http://atlshows.neocities.org/
NAME:ATL Shows
X-WR-CALNAME:ATL Shows
DESCRIPTION:Shows in Atlanta GA
X-WR-CALDESC:Shows in Atlanta GA
TIMEZONE-ID:America/New_York
REFRESH-INTERVAL;VALUE=DURATION:PT12H
X-PUBLISHED-TTL:PT12H
COLOR:186:218:85
CALSCALE:GREGORIAN
METHOD:PUBLISH`;



    shows.forEach((show, i) => {
        /**
        "headliner": "The Movement",
        "support": "KBong,Johnny Cosmic",
        "date": "2024-06-08 (Sat)",
        "price": "$29.50",
        "venue": "Variety Playhouse",
        "masquerade presents?": "N"
         */
        let showDescription = `${show.support} - ${show.price}`;
        // 75 max length - 12 for keyname - 3 for lil dots
        if (showDescription.length > 60) {
            showDescription = showDescription.substring(0, 59) + '...';
        }
        let showSummary = show.headliner;
        // 75 max length - 8 for keyname - 3 for lil dots
        if (showSummary.length > 64) {
            showSummary = showSummary.substring(0, 63) + '...';
        }
        const showDateParts = show.date.split('-');
        const showDate = `${showDateParts[0]}${showDateParts[1]}${showDateParts[2].split(' ')[0]}`;
        calendarText += `\nBEGIN:VEVENT\nUID:atlshows-${i}
DTSTAMP:${showDate}
DTSTART:${showDate}
DTEND:${showDate}
SUMMARY:${showSummary}
DESCRIPTION:${showDescription}
LOCATION:${show.venue}
URL:${getVenueUrl(show.venue)}
END:VEVENT`;
    });
    // close calendar
    return calendarText + `\nEND:VCALENDAR`
}

let shows = JSON.parse(fs.readFileSync(SRC_DATA, 'utf-8'))
fs.writeFileSync('site/atl-shows.ics', calendarFromShows(shows), 'utf8');