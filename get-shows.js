const path = require('path');
const fs = require('fs');
const https = require('https')
const html2parser = require('htmlparser2/lib/WritableStream');
const _ = require('lodash');

const DAYS = [
    'Sun', 'Mon', 'Tue', 'Wed',
    'Thu', 'Fri', 'Sat'
];

const dataPath = path.join(__dirname, 'data');
if (!fs.existsSync(dataPath)) {
    fs.mkdirSync(dataPath);
}

const msg = 'Loaded shows';

console.time(msg);
run().then(function () {
    console.timeEnd(msg);
}, function (err) {
    throw err;
});

async function run(attempts) {
    const five29Shows = get529Shows();
    const earlshows = getTheEarlShows();
    const mshows = getMasqueradeShows();
    const vshows = getVarietyPlayhouseShows();
    const eshows = getTheEasternShows();
    const twshows = getTerminalWestShows();
    const cstShows = getCSTShows();
    const sbShows = getStarBarShows();
    const edShows = getEyedrumShows();
    const boggShows = getBoggsSSShows();
    const districtShows = getDistrictShows();
    const aisle5Shows = getAisle5Shows();
    const tabernacleShows = getTabernacleShows();
    const buckheadTheater = getBuckheadTheaterShows();

    const today = displayDate(new Date());
    let emptyResults = 0;

    let combined = _.chain(
        await Promise.allSettled([
            mshows, vshows, eshows, twshows, earlshows,
            five29Shows, cstShows, sbShows, boggShows,
            districtShows, aisle5Shows, edShows,
            tabernacleShows, buckheadTheater
        ])
    )
        .filter(result => result.status === 'fulfilled')
        .map(result => {
            if (result.status == 'rejected') {
                console.log('Site failed');
                emptyResults++;
            }
            try {
                if (result.value.length === 0) {
                    emptyResults++;
                }
            } catch (e) {
                emptyResults++;
            }
            return result.value;
        })
        .flatten().sortBy(['date', 'venue'])
        .filter(show => show.date >= today)
        .value();

    // remove duplicates
    let deduped = _.chain(combined)
        .uniqBy(item => item.date + item.headliner)
        .values()
        .flatten()
        .value();

    if (deduped.length === 0 || (emptyResults > 0 && deduped.length < 400)) {
        console.log('empty results', emptyResults);
        if (!attempts) {
            attempts = 0;
        }
        attempts += 1;
        if (attempts > 10) {
            throw new Error('Something is busted RIP');
        }
        console.log(new Date().toJSON(), 'No shows! Retrying... attempt', attempts);
        await holdOn(30000);
        return run(attempts);
    }
    // track diffs
    const OUTPUT_DEST = path.join(dataPath, `all-shows.json`);
    if (fs.existsSync(OUTPUT_DEST)) {
        const lastUpdateData = require(OUTPUT_DEST);
        const today = new Date().toISOString().split('T').shift()
        const diff = [];
        // check for show updates and removals other than past entries
        lastUpdateData.forEach(oldShow => {
            if (oldShow.date < today) {
                return;
            }
            var stillExists = deduped.find(newShow => {
                return newShow.date === oldShow.date &&
                    newShow.headliner === oldShow.headliner;
            });
            if (stillExists && (oldShow.venue != stillExists.venue ||
                oldShow.price != stillExists.price ||
                oldShow.support != stillExists.support)) {
                diff.push({
                    diffType: 'update',
                    prev: oldShow,
                    next: stillExists
                });
            } else if (!stillExists) {
                diff.push({
                    diffType: 'removed',
                    prev: oldShow
                });
            }
        });
        // check for new shows
        deduped.forEach(newShow => {
            var existed = lastUpdateData.find(oldShow => {
                return newShow.date === oldShow.date &&
                    newShow.headliner === oldShow.headliner;
            });
            if (!existed) {
                diff.push({
                    diffType: 'new',
                    next: newShow
                });
            }
        });
        // erase old data
        fs.unlinkSync(OUTPUT_DEST);

        // create diff file
        const DIFF_DEST = path.join(dataPath, `diff-${today}.json`);
        if (fs.existsSync(DIFF_DEST)) {
            fs.unlinkSync(DIFF_DEST)
        }
        if (diff.length) {
            console.log(`Recording ${diff.length} diffs`);
            fs.writeFileSync(DIFF_DEST, JSON.stringify(diff));
        }
    }

    fs.writeFileSync(OUTPUT_DEST, JSON.stringify(deduped));

}

async function getAEGShows(url) {
    const data = await doReq(new URL(url), {
        headers: {
            Accept: 'application/json, text/javascript, */*; q=0.01',
        },
        json: true
    });

    return data.events.filter(evt => evt.ticketing.status !== 'Cancelled').map(mapAEGShowObj);
}

/**
 * Gather upcoming Variety Playhouse shows
 * @returns Promise<Shows[]>
 */
async function getVarietyPlayhouseShows() {
    const vpURL = 'https://aegwebprod.blob.core.windows.net/json/events/214/events.json'
    const result = await getAEGShows(vpURL);
    if (!result.length) {
        console.log('Failed to get VarietyPlayhouseShows');
    }
    return result;
}

// The eastern
async function getTheEasternShows() {
    const EURL = 'https://aegwebprod.blob.core.windows.net/json/events/127/events.json';
    const result = await getAEGShows(EURL);
    if (!result.length) {
        console.log('Failed to get TheEasternShows');
    }
    return result;
}

// terminal west
async function getTerminalWestShows() {
    const EURL = 'https://aegwebprod.blob.core.windows.net/json/events/211/events.json';
    const result = await getAEGShows(EURL);
    if (!result.length) {
        console.log('Failed to get TerminalWestShows');
    }
    return result;
}

function mapAEGShowObj(evt) {
    let headliner = '';
    let support = '';

    if (evt.associations.headliners.length) {
        headliner = evt.associations.headliners.map(headliner => {
            return headliner.name;
        }).join();
    } else {
        headliner = evt.title.headliners.split('\">').pop().split('</\"').shift();
    }

    if (evt.associations.supportingActs.length) {
        support = evt.associations.supportingActs.map(act => {
            return act.name;
        }).join();
    } else if (evt.title.supporting) {
        support = evt.title.supporting.split('\">').pop().split('</\"').shift();
    }


    const row = {
        headliner,
        support,
        date: displayDate(evt.eventDateTime),
        price: evt.ticketPrice || evt.doorPrice || 'TBD',
        venue: evt.venue.title,
        'masquerade presents?': evt.title.presentedBy && evt.title.presentedBy.includes('The Masquerade') ? 'Y' : 'N'
    };
    return row;
}

// Center Stage Theater/Vinyl/The Loft
async function getCSTShows() {
    const cURL = 'https://www.centerstage-atlanta.com/wp-json/centerstage/v2/events/?venue=all';
    const data = await doReq(new URL(cURL), {
        headers: {
            Accept: 'application/json, text/javascript, */*; q=0.01',
        },
        json: true
    });
    const mapped = data.map(evt => {
        let headliner = evt.title;
        if (evt.show_time_status) {
            headliner = `${headliner} (${evt.show_time_status})`;
        }

        let support = '';
        if (evt.supporting_artists_info) {
            support = _.chain(evt.supporting_artists_info)
                .map('supporting_artist_name')
                .filter()
                .uniq()
                .value().join(', ');
        }

        let evtDate = displayDate(`${evt.event_date.slice(0, 4)}-${evt.event_date.slice(4, 6)}-${evt.event_date.slice(6, 8)}T16:20:00`);

        let price = '';
        if (evt.price_range) {
            price = evt.price_range.split('/').shift();
        }

        let venue = '';
        if (evt.venue_room) {
            venue = evt.venue_room.label
        } else if (evt.external_venue) {
            venue = evt.external_venue;
        }
        if (evt.show_status && evt.show_status.value && evt.show_status.value !== 'Buy Tickets') {
            venue = `(${evt.show_status.value}) ${venue}`;
        }


        const row = {
            headliner: evt.title,
            support,
            date: displayDate(evtDate),
            price: price || 'TBD',
            venue: venue,
            'masquerade presents?': evt.promoter.toLowerCase().includes('masquerade') ? 'Y' : 'N'
        };
        return row;
    }).filter(row => row.venue);
    if (!mapped.length) {
        console.log('Failed to get CSTShows');
    }
    return mapped;
}

/**
 * Gather upcoming Masquerade shows
 * @returns Promise<Show[]>
 */
function getMasqueradeShows() {
    const mURL = 'http://www.masqueradeatlanta.com/events/';
    return new Promise(async (resolve, reject) => {
        // get HTML response stream
        let res;
        try {
            res = await doReq(new URL(mURL), {
                streamResponse: true
            });
        } catch (e) {
            return reject(e);
        }

        let currentShow = {};
        const allShows = [];

        function publishShow(show) {
            if (show.buyButton === 'CANCELED') {
                return;
            }
            if (show.venueText) {
                show.venue = show.venueText;
            }
            if (show.buyButton === 'SOLD OUT' && show.price && !show.price.toLowerCase().includes('sold out')) {
                show.venue = `(SOLD OUT) ${show.venue}`;
            }
            delete show.venueText;
            delete show.buyButton;
            // copy existing object and add to list
            if (show.headliner) {
                allShows.push(Object.assign({}, show));
            }
        }
        // pipe stream to HTML parser
        const parser = new html2parser.WritableStream({
            onopentag(name, attribs) {
                // track open tags
                if (name === 'section' && attribs.class === 'eventDetails') {
                    // new section, publish current show
                    if (Object.keys(currentShow > 1)) {
                        publishShow(currentShow);
                    }
                    currentShow = {
                        headliner: false,
                        support: '',
                        date: false,
                        price: false,
                        venue: false,
                        'masquerade presents?': 'Y'
                    };
                }
                if (name === 'div' && attribs.class === 'eventStartDate') {
                    currentShow.date = displayDate(attribs.content);
                }
                if (name === 'a' && attribs.class === 'btn btn-purple btn-full') {
                    currentShow.buyButton = true;
                }
                if (name === 'h2' && attribs.class && attribs.class.includes('eventHeader__title')) {
                    currentShow.headliner = true;
                }
                if (name === 'h4' && attribs.class && attribs.class.includes('eventHeader__support')) {
                    currentShow.support = true;
                }
                if (name === 'span' && attribs.class === 'js-listVenue') {
                    currentShow.venue = true;
                } else if (name === 'p' && attribs.class === 'event__location-room') {
                    currentShow.venue = true;
                } else if (currentShow.venue && name === 'span' && attribs.class === 'sr-only') {
                    currentShow.venue = true;
                } else {
                    currentShow.venue = false;
                }
                if (name === 'div' && attribs.class === 'time-show') {
                    currentShow.price = true;
                }
            },
            ontext(text) {
                // extract text from open tags
                if (currentShow.headliner === true) {
                    currentShow.headliner = text.trim();
                }
                if (currentShow.support === true) {
                    currentShow.support = text.trim();
                }
                if (currentShow.venue === true) {
                    if (!currentShow.venueText) {
                        currentShow.venueText = '';
                    }
                    currentShow.venueText += text.trim()
                        .replace('at The Masquerade', ' - Masquerade')
                        .replace('\n', '')
                        .replace('Other Location', '');
                }
                if (currentShow.price === true) {
                    currentShow.price = text.trim().split(' / ')[1];
                }
                if (currentShow.buyButton === true) {
                    currentShow.buyButton = text.trim();
                }
            },
            onend() {
                // send last show to list
                publishShow(currentShow);
                // return the shows
                if (!allShows.length) {
                    console.log('faile to get MasqueradeShows');
                }
                resolve(allShows);
            }
        }, {
            decodeEntities: false
        });

        res.pipe(parser);
    });
}

async function getTheEarlShows() {
    const eurl = 'https://badearl.com/';
    let currentPage = 0;
    let pages = [1];
    let allShows = [];
    let currentUrl = eurl;

    while (currentPage < pages.length) {
        if (currentPage) {
            currentUrl = `https://badearl.com/?sf_paged=${pages[currentPage]}`;
        }
        const presults = await nextPage();
        allShows = _.union(allShows, presults);
        currentPage += 1;
    }

    return allShows;

    function nextPage() {
        return new Promise(async (resolve, reject) => {
            const results = [];
            let currentShow = {};

            function publishShow(show) {
                // get a JS Date parse-able date and time
                let dateTimeStr = `${show.date} ${show.time}`
                    .replace(' show', '')
                    .replace('pm', ' pm')
                    .replace('.', '')
                    .split('y, ').pop();

                delete show.time;
                show.date = displayDate(dateTimeStr);
                show.venue = 'The EARL';
                show['masquerade presents?'] = 'N';
                results.push(show);
            }

            let res;
            try {
                res = await doReq(new URL(currentUrl), {
                    streamResponse: true
                });
            } catch (e) {
                return reject(e);
            }

            const parser = new html2parser.WritableStream({
                onopentag(name, attribs) {
                    if (name === 'div' && attribs.class && attribs.class.startsWith('cl-layout__item')) {
                        if (Object.keys(currentShow).length > 1) {
                            publishShow(currentShow);
                            // reset show object
                            currentShow = {};
                        }
                    }

                    if (name === 'a' && attribs.class === 'page-numbers' && attribs.href &&
                        attribs.href.includes('/?sf_paged=')) {
                        const num = parseInt(attribs.href.split('?sf_paged=').pop());
                        if (num > currentPage && !pages.includes(num)) {
                            pages.push(num);
                        }
                    }
                    // headliner tags, can have multiple of each
                    if (name === 'h1' && attribs.class && attribs.class.includes('show-listing-title')) {
                        currentShow.headlinerLine = true;
                    }
                    if (name === 'div' && attribs.class && attribs.class.includes('show-listing-headliner')) {
                        currentShow.headlinerLine = true;
                    }
                    // support tag, multiples possible
                    if (name === 'div' && attribs.class && attribs.class.includes('show-listing-support')) {
                        currentShow.supportLine = true;
                    }
                    // date
                    if (name === 'p' && attribs.class === 'show-listing-date') {
                        currentShow.dateLine = true;
                    }
                    // time
                    if (name === 'p' && attribs.class === 'show-listing-time') {
                        currentShow.timeLine = true;
                    }
                    // price, get adv only
                    if (name === 'p' && attribs.class === 'show-listing-price') {
                        currentShow.priceLine = true;
                    }
                },
                ontext(text) {
                    // extract text from open tags
                    if (currentShow.headlinerLine === true) {
                        currentShow.headliner = currentShow.headliner || '';
                        currentShow.headliner = _.chain([currentShow.headliner])
                            .union([text.trim()])
                            .filter()
                            .value()
                            .join();
                        delete currentShow.headlinerLine;
                    }
                    if (currentShow.supportLine === true) {
                        currentShow.support = currentShow.support || '';
                        currentShow.support = _.chain([currentShow.support])
                            .union([text.trim()])
                            .filter()
                            .value()
                            .join();
                        delete currentShow.supportLine;
                    }
                    if (currentShow.dateLine === true) {
                        currentShow.date = text.trim();
                        delete currentShow.dateLine;
                    }
                    if (currentShow.timeLine === true) {
                        if (text.includes(' show')) {
                            currentShow.time = text.trim();
                        }
                        delete currentShow.timeLine;
                    }
                    if (currentShow.priceLine === true) {
                        if (text.includes(' ADV')) {
                            currentShow.price = text.trim()
                                .split(' ADV').shift();
                        }
                        delete currentShow.priceLine;
                    }
                },
                onend() {
                    publishShow(currentShow);
                    if (!results.length) {
                        console.log('Failed to get Earl shows');
                    }
                    resolve(results)
                },
            }, {
                decodeEntities: false
            });
            res.pipe(parser);
        });
    }
}

async function get529Shows() {
    const eurl = 'http://529atlanta.com/calendar/';
    let currentPage = 0;
    let pages = [1];
    let allShows = [];
    let currentUrl = eurl;

    // @todo no paging on the site as of 3/25/22
    while (currentPage < pages.length) {
        const presults = await nextPage();
        allShows = _.union(allShows, presults);
        currentPage += 1;
    }

    return allShows;

    function nextPage() {
        return new Promise(async (resolve, reject) => {
            const results = [];
            let currentShow = {};

            function publishShow(show) {
                show.date = displayDate(`${show.date} ${show.time}`);
                delete show.time;
                show.venue = '529';
                show['masquerade presents?'] = 'N';
                results.push(show);
            }

            let res;
            try {
                res = await doReq(new URL(currentUrl), {
                    streamResponse: true,
                    rejectUnauthorized: false
                });
            } catch (e) {
                return reject(e);
            }

            const nlregex = /\\r\\n/g;

            const parser = new html2parser.WritableStream({
                onopentag(name, attribs) {
                    if (name === 'div' && attribs.class && attribs.class.startsWith('event-container-single')) {
                        if (Object.keys(currentShow).length > 1) {
                            publishShow(currentShow);
                            // reset show object
                            currentShow = {};
                        }
                    }

                    if (name === 'span' && attribs.class === 'right') {
                        currentShow.dateLine = true
                    }
                    if (name === 'p' && attribs.class === 'event-meta') {
                        // time and price in the same text
                        currentShow.timeLine = true;
                    }
                    // headliner tags, can have multiple of each
                    if (name === 'h3' && attribs.class === 'event-headliners') {
                        currentShow.headlinerLine = true;
                    }
                    // support tag, multiples possible
                    if (name === 'h4' && attribs.class === 'event-bands') {
                        currentShow.supportLine = true;
                    }
                },
                ontext(text) {
                    // extract text from open tags
                    if (currentShow.headlinerLine === true) {
                        currentShow.headliner = currentShow.headliner || '';
                        currentShow.headliner = _.chain([currentShow.headliner])
                            .union([text.trim()])
                            .filter()
                            .value()
                            .join(', ');
                        delete currentShow.headlinerLine;
                    }
                    if (currentShow.supportLine === true) {
                        currentShow.support = currentShow.support || '';
                        const items = text.split('|').map(val => val.trim());
                        currentShow.support = _.chain([currentShow.support])
                            .union(items)
                            .filter()
                            .value()
                            .join(', ');
                        delete currentShow.supportLine;
                    }
                    if (currentShow.dateLine === true) {
                        currentShow.date = text.trim();
                        delete currentShow.dateLine;
                    }
                    if (currentShow.timeLine === true) {
                        const items = text.replace(nlregex, '')
                            .trim().split('|')
                            .map(item => item.trim())
                        items.forEach(item => {
                            if (item.includes('am') || item.includes('pm')) {
                                currentShow.time = item;
                            }
                            if (item.includes('$')) {
                                currentShow.price = item;
                            }
                        });
                        delete currentShow.timeLine;
                    }
                },
                onend() {
                    publishShow(currentShow);
                    if (!results.length) {
                        console.log('Failed to get 529 shows');
                    }
                    resolve(results);
                },
            }, {
                decodeEntities: false
            });
            res.pipe(parser);
        });
    }
}

async function getStarBarShows() {
    const result = await getSquarespaceSite({
        siteURL: 'https://www.starbaratl.bar/shows',
        venueName: 'Star Bar',
        includeSupport: true
    });
    if (!result.length) {
        console.log('Failed to get StarBarShows');
    }
    return result;
}

async function getEyedrumShows() {
    const result = await getSquarespaceSite({
        siteURL: 'https://www.eyedrum.org/calendar-events-performances-art-music',
        venueName: 'eyedrum',
        includeSupport: false
    });
    if (!result.length) {
        console.log('Failed to get EyedrumShows');
    }
    return result;
}

async function getBoggsSSShows() {
    const result = await getSquarespaceSite({
        siteURL: 'https://www.boggssocial.com/events',
        venueName: 'Boggs SS',
        includeSupport: false
    });
    if (!result.length) {
        console.log('Failed to get BoggsSSShows');
    }
    return result;
}

async function getSquarespaceSite(options) {
    const siteURL = options.siteURL;
    const venueName = options.venueName;
    const includeSupport = options.includeSupport;

    let currentPage = 0;
    let pages = [1];
    let allShows = [];
    let currentUrl = siteURL;

    // @todo no paging on the site as of 3/25/22
    while (currentPage < pages.length) {
        const presults = await nextPage();
        allShows = _.union(allShows, presults);
        currentPage += 1;
    }

    return allShows;

    function nextPage() {
        return new Promise(async (resolve, reject) => {
            const results = [];
            let currentShow = {};

            function publishShow(show) {
                delete show.supportLine;
                show.date = displayDate(show.date);
                delete show.time;
                show.venue = venueName;
                show['masquerade presents?'] = 'N';
                results.push(show);
            }

            let res;
            try {
                res = await doReq(new URL(currentUrl), {
                    streamResponse: true,
                    rejectUnauthorized: false,
                    headers: {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:100.0) Gecko/20100101 Firefox/100.0'
                    }
                });
            } catch (e) {
                return reject(e);
            }

            let upcomingShows = false;

            const parser = new html2parser.WritableStream({
                onopentag(name, attribs) {
                    if (name === 'div' && attribs.class === 'eventlist eventlist--upcoming') {
                        upcomingShows = true;
                    }
                    if (!upcomingShows) {
                        return;
                    }
                    if (name === 'article' && attribs.class && attribs.class.includes('eventlist-event')) {
                        if (Object.keys(currentShow).length > 1) {
                            publishShow(currentShow);
                            // reset show object
                            currentShow = {};
                        }
                    }

                    if (attribs.class === 'event-date' && !currentShow.date) {
                        currentShow.dateLine = true
                    }
                    // headliner tag
                    if (attribs.class === 'eventlist-title-link') {
                        currentShow.headlinerLine = true;
                    }
                    // @note support, time, and price all in the same kind of tag
                    if (!includeSupport) {
                        return;
                    }
                    if (currentShow.supportActive && name === 'p') {
                        currentShow.supportLine = true;
                    } else if (name === 'div' && attribs.class === 'eventlist-excerpt') {
                        // inner support line
                        currentShow.supportActive = true;
                    } else {
                        delete currentShow.supportActive;
                    }
                },
                ontext(text) {
                    // extract text from open tags
                    if (currentShow.headlinerLine === true) {
                        currentShow.headliner = currentShow.headliner || '';
                        currentShow.headliner = _.chain([currentShow.headliner]).union([text.trim()]).filter().value().join(', ');
                        delete currentShow.headlinerLine;
                    }
                    if (currentShow.supportLine === true) {
                        currentShow.support = currentShow.support || '';
                        if (text.includes('pm door')) {
                            currentShow.time = text;
                        } else if (text.startsWith('$')) {
                            currentShow.price = text;
                        } else if (text !== 'CLICK FOR TIX' && !text.includes('PROOF OF COVID VACCINATION ')) {
                            currentShow.support = _.chain([currentShow.support])
                                .union([text.trim()]).uniq().filter().value().join(', ');
                        }
                    }
                    if (currentShow.dateLine === true) {
                        currentShow.date = text.trim();
                        delete currentShow.dateLine;
                    }
                },
                onend() {
                    publishShow(currentShow);
                    resolve(results)
                },
            }, {
                decodeEntities: false
            });
            res.pipe(parser);
        });
    }
}

async function getDistrictShows() {
    const durl = `https://districtatlanta.com/events/`;
    const venueName = 'District';

    return new Promise(async (resolve, reject) => {
        const results = [];

        function publishShow(show) {
            show.venue = venueName;
            show['masquerade presents?'] = 'N';
            results.push(show);
        }

        let res;
        try {
            res = await doReq(new URL(durl), {
                streamResponse: true,
                rejectUnauthorized: false,
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:100.0) Gecko/20100101 Firefox/100.0'
                }
            });
        } catch (e) {
            return reject(e);
        }

        const parser = new html2parser.WritableStream({
            onopentag(name, attribs) {
                if (name === 'a' && attribs.class && attribs.class === 'ps-2' &&
                    attribs.href && attribs.href.endsWith('.php')) {

                    let hrefParts = attribs.href.replace('.php', '').split('-');
                    hrefParts.reverse();
                    let yyyy = hrefParts[0];
                    let dth = hrefParts[1].replace('th', '');
                    let mth = hrefParts[2].replace('th', '');
                    let dstr = `${mth} ${dth} ${yyyy}`;
                    publishShow({
                        headliner: attribs.title,
                        date: displayDate(dstr)
                    });
                }
            },
            onend() {
                if (!results.length) {
                    console.log('Faile to get DistrictShows');
                }
                resolve(results);
            },
        }, {
            decodeEntities: false
        });
        res.pipe(parser);
    });
}

async function getAisle5Shows() {
    const aurl = `https://aisle5atl.com/`;
    const yesterday = new Date(new Date().setDate(new Date().getDate() - 1));
    const todayYear = yesterday.getFullYear();

    return new Promise(async (resolve, reject) => {
        const results = [];
        let currentShow = {};

        function publishShow(show) {
            delete show.supportLine;
            show.date = displayDate(show.date);
            delete show.time;
            show.venue = 'Aisle 5';
            show['masquerade presents?'] = 'N';
            results.push(show);
        }

        let res;
        try {
            res = await doReq(new URL(aurl), {
                streamResponse: true,
                rejectUnauthorized: false,
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:100.0) Gecko/20100101 Firefox/100.0'
                }
            });
        } catch (e) {
            return reject(e);
        }

        let upcomingShows = false;

        const parser = new html2parser.WritableStream({
            onopentag(name, attribs) {
                if (attribs.class && attribs.class.includes('seetickets-list-event-content-container')) {
                    upcomingShows = true;
                }
                if (!upcomingShows) {
                    return;
                }
                if (name === 'a' && attribs.href &&
                    attribs.href.includes('afflky=Aisle5') &&
                    attribs.class && attribs.class.includes('button-gettickets')) {
                    let value = attribs['aria-label']
                    // Buy Tickets for sign crushes motorist on Jul 08
                    //   -> 202(4|5)/07/08
                    let dateParts = value.split(' on ').pop().split(' ');
                    let date = new Date(`${dateParts[0]} ${dateParts[1]} ${todayYear}`);
                    if (date < yesterday) {
                        date = new Date(`${dateParts[0]} ${dateParts[1]} ${todayYear + 1}`);
                    }
                    // Buy Tickets for sign crushes motorist on Jul 08
                    //   -> sign crushes motorist
                    let headliner = value.split('Buy Tickets for ').pop().split(' on ').shift();

                    currentShow.headliner = headliner;
                    currentShow.date = date;
                    if (Object.keys(currentShow).length > 1) {
                        publishShow(currentShow);
                    }
                    // reset show object
                    currentShow = {};
                }

                if (attribs.class && attribs.class.includes('supporting-talent')) {
                    currentShow.supportLine = true;
                }

                if (attribs.class && attribs.class.includes('price')) {
                    currentShow.priceLine = true;
                }
            },
            ontext(text) {
                // extract text from open tags
                if (currentShow.supportLine === true) {
                    currentShow.support = text.trim();
                    delete currentShow.supportLine;
                }
                if (currentShow.priceLine === true) {
                    currentShow.price = text.trim();
                    delete currentShow.priceLine;
                }
            },
            onend() {
                publishShow(currentShow);
                if (!results.length) {
                    console.log('Faile to get Aisle5Shows');
                }
                resolve(results);
            },
        }, {
            decodeEntities: false
        });
        res.pipe(parser);
    });
}

async function liveNationNextJS(url) {
    const res = await doReq(new URL(url));
    const jsLinkPattern = /\/_next\/static\/chunks\/([a-z0-9]){3}-([a-z0-9]){16}.js\?dpl=([a-z-A-Z0-9_])+/gm;
    const apiKeyPattern = /GraphQL:{apiKey:\"[a-z0-9]{3}-[a-z0-9]{26}\"/gm;
    const venueIdPattern = /name="venue_id" value="([a-zA-Z])\w+"/gm;
    // iterate through JS links matching pattern
    const jsFileURLs = res.match(jsLinkPattern);
    let apiKey;
    let pageOffset = 0;
    for (let i = 0; i < jsFileURLs.length; i++) {
        // get JS file
        const jsURL = new URL(jsFileURLs[i], url);
        const jsContent = await doReq(jsURL);
        const keyMatch = jsContent.match(apiKeyPattern)
        // search for NextJS API key
        if (keyMatch) {
            apiKey = keyMatch[0].split(":\"")[1].split('"').shift();
            break;
        }
    }
    if (!apiKey) {
        console.error('Failed to get TabernacleShows');
        return [];
    }
    // get Venue ID
    const venueIdMatches = res.match(venueIdPattern);
    const venueId = venueIdMatches[0].split('value="').pop().split('"').shift();
    // get shows
    const livenationURL = 'https://api.livenation.com/graphql';
    let allParsed = [];
    let moreResults = true;
    let offset = 0;
    while (moreResults) {
        const result = await doReq(new URL(livenationURL), {
            headers: {
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:130.0) Gecko/20100101 Firefox/130.0',
                'x-api-key': apiKey
            },
            method: 'POST',
            data: `{\"query\":\"query EVENTS_PAGE($offset: Int!, $venue_id: String!, $include_genres: String, $start_date_time: String, $end_date_time: String) {\\n  getEvents(\\n    filter: {exclude_status_codes: [\\\"cancelled\\\", \\\"postponed\\\"], image_identifier: \\\"RETINA_PORTRAIT_16_9\\\", venue_id: $venue_id, start_date_time: $start_date_time, end_date_time: $end_date_time, include_genres: $include_genres}\\n    limit: 36\\n    offset: $offset\\n    order: \\\"ascending\\\"\\n    sort_by: \\\"start_date\\\"\\n  ) {\\n    artists {\\n      discovery_id\\n      name\\n      slug\\n      images {\\n        fallback\\n        image_url\\n      }\\n      genre_id\\n      genre\\n    }\\n    discovery_id\\n    event_date\\n    event_date_timestamp_utc\\n    event_end_date\\n    event_end_time\\n    event_status_code\\n    event_time\\n    event_timezone\\n    images {\\n      fallback\\n      image_url\\n    }\\n    name\\n    upsell {\\n      classification_id\\n      discovery_id\\n      name\\n      type\\n      url\\n    }\\n    url\\n    venue {\\n      name\\n      discovery_id\\n      location {\\n        address\\n        city\\n        country\\n        latitude\\n        longitude\\n        state\\n        zip_code\\n      }\\n    }\\n  }\\n}\\n\",\"variables\":{\"offset\":${offset},\"venue_id\":\"${venueId}\"}}`
        });
        const parsed = JSON.parse(result);
        allParsed = allParsed.concat(parsed.data.getEvents);
        offset += 36;
        if (parsed.data.getEvents.length < 36) {
            moreResults = false;
        }
    }
    return allParsed.map(entry => {
        let venue = entry.venue.name;
        if (entry.event_status_code != 'onsale') {
            venue += ` (${entry.event_status_code})`;
        }
        return {
            date: displayDate(`${entry.event_date}T00:00`),
            headliner: entry.name,
            support: entry.artists.filter(artist => {
                return entry.name.toLowerCase()
                    .indexOf(artist.name.toLowerCase()) == -1;
            }).map(artist => artist.name).join(', '),
            venue: venue,
            price: '',
            link: entry.url
        };
    });
}

async function getTabernacleShows() {
    const url = 'https://www.tabernacleatl.com/shows';
    return await liveNationNextJS(url);
}

async function getBuckheadTheaterShows() {
    const url = 'https://www.thebuckheadtheatre.com/shows';
    return await liveNationNextJS(url);
}

function displayDate(dateString) {
    const jsDate = new Date(dateString);
    const result = `${jsDate.getFullYear()}-${String(jsDate.getMonth() + 1).padStart(2, '0')}-${String(jsDate.getDate()).padStart(2, '0')} (${DAYS[jsDate.getDay()]})`;
    if (result.includes('NaN')) {
        return '';
    }
    return result;
}

function doReq(url, opts) {
    opts = opts || {};
    return new Promise((resolve, reject) => {
        const options = {
            hostname: url.hostname,
            port: url.port,
            path: url.pathname + url.search,
            method: opts.method || 'GET',
            headers: opts.headers || {},
            rejectUnauthorized: opts.rejectUnauthorized === undefined ? true : opts.rejectUnauthorized
        };

        const req = https.request(options, res => {
            let output = '';
            if (opts.streamResponse) {
                return resolve(res);
            }
            res.on('data', d => {
                output += d;
            });
            res.on('end', () => {
                if (opts.json) {
                    try {
                        resolve(JSON.parse(output));
                    } catch (e) {
                        reject(e);
                    }
                } else {
                    resolve(output);
                }
            });
        });

        req.on('error', error => {
            reject(error);
        });

        if (opts.data) {
            req.write(opts.data);
        }

        req.end();
        return req;
    });
}

function holdOn(timeoutMS) {
    return new Promise(resolve => {
        setTimeout(resolve, timeoutMS);
    });
}