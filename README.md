# ATL Shows scraper
Code for gathering data and building [this site](https://atlshows.neocities.org/).

Tracking shows from
- Variety Playhouse
- Masquerade
- The Eastern
- Terminal West
- The Earl
- 529
- Boggs Social and Supply
- Star Bar
- Eyedrum
- Aisle 5
- Center Stage Theater/The Loft/Vinyl
- District
- Tabernacle
- Buckhead Theatre

### Sync neocities
Sync shows
```sh
npm install
node get-shows.js
node create-page.js
```

See [Neocities CLI](https://blog.neocities.org/blog/2017/05/26/neocities-cli)
```sh
sudo gem install neocities
# log in
neocities info your-site
# answer login prompts
# do a dry run
neocitie push --dry-run site/
```